import lib.MQTTConnection as mqtt
import threading
import json
import serial
import time
import lib.uart.utils as uartlib
import lib.uart.uart_conn as uart_conn
import lib.sql.db as db


import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import lib.ui.imageButton as imgbtn
import lib.ui.fancytabbar as tabwg
import lib.ui.controlWidget as ctrlwg

class uartThread(QThread):
    ctrl_sig=pyqtSignal(str,int,name="ctrsignal")
    get_all_device_sig=pyqtSignal(list,name="getall")
    uart_sig=pyqtSignal(int,name="uartsignal")
    uart_update_power=pyqtSignal(str,int,name="uartpower")
    def __init__(self):
        super(uartThread, self).__init__()
        # self.ser = serial.Serial ("/dev/ttyAMA0")    #Open named port
        #self.ser = serial.Serial("/dev/ttyUSB0")  # Open named port
        #self.ser = serial.Serial('COM5')    #Open named port
        #self.uart_timer = QTimer(self)
        #self.uart_timer.timeout.connect(self.tryOpenSerial)
        #self.getSerialOrNone('COM5')
        #self.uart_timer.start(1000)

        #self.list_device=[{'status': 0, 'addr': '00124B000A91644B1'}, {'status': 1, 'addr': '00124B000A91644B2'}, {'status': 1, 'addr': '00124B000A91644B3'}]
        self.list_device=[]
        #self.start()
        self.Init_Timer()
        self.uart=uart_conn.Uart()
        self.uart.uart_sig.connect(self.Uart_Signal_Handle)
        self.uart.uart_sig1.connect(self.Uart_Signal_Handle)
        self.uart.uart_ctr.connect(self.Handle_Cmd)
        self.uart.start()

        self.timer3 = QTimer(self)
        self.timer3.timeout.connect(self.Simulation)
        #self.timer3.start(2000)

    def Simulation(self):
        self.Handle_Cmd("@ZBR=6;00124B000A91644B;1,2!")

    def Init_Timer(self):
        self.timer1 = QTimer(self)
        self.timer1.timeout.connect(self.Update_Status_All_Device)
    def Uart_Signal_Handle(self,cmd):
        print(cmd)
        if cmd==2:
            self.uart_sig.emit(5)  #Reconnect
        if cmd==6:
            self.Get_All_Device()


    def Serial_Write(self, data):
        self.uart.Serial_Write(data)

    '''
    Opcode:
    GetAllStatus: 1
    MainControl: 2
    ZBEndDeviceControl: 5
    ZBSendPower: 6
    '''
    def Handle_Cmd(self,buffer):

        x=uartlib.Parse_To_Cmd(buffer)
        if x != None:
            if x["cmd"] == 1:
                ret = uartlib.Zigbee_Get_All_Device(x["addr"], x["val"])
                self.list_device+=ret
            if x["cmd"] == 5:
                ret = uartlib.Zigbee_Control(x["addr"], x["val"])
                print(ret)
                self.ctrl_sig.emit(ret["addr"], ret["status"])

            if x["cmd"] == 2:
                ret = uartlib.Zigbee_Control(x["addr"], x["val"])
                self.ctrl_sig.emit(ret["addr"], ret["status"])

            if x["cmd"] == 6:
                ret = uartlib.Zigbee_Update_Power(x["addr"], x["val"])
                self.uart_update_power.emit(ret["addr"],ret["power"])
                print(ret)




    def Send_Control_Signal_To_Serial(self,did,status):
        payload="@ZBC=2;"
        addr=did[:len(did)-1]
        offset = did[len(did) - 1:]
        payload+=addr+";"+offset+str(status)+"!"
        self.Serial_Write(payload)
        #print("send to serial:",payload)

        self.ctrl_sig.emit(did,status)

    def Get_All_Device(self):
        self.timer1.stop()
        self.Serial_Write("@ZBC=1!")
        self.timer1.start(5000)

    def Update_Status_All_Device(self):
        print("update stt list device",self.list_device)
        self.timer1.stop()
        self.get_all_device_sig.emit(self.list_device)



class mainWindow(QMainWindow):
    #btnsig = pyqtSignal(str, str, int, name="btnsig")
    sig = pyqtSignal(str,str,int)
    btn_click_sig = pyqtSignal(str, str, int, name="btnsig")

    def __init__(self):
        super().__init__()

        self.Init_UI()
        self.resize(800,480)

    def Sync_Status(self,hid,did,status):
        print("Sync stt:",did)
        for dev in self.tab2.ldevice:
            if dev.Get_Device_Id()==did:
                dev.Sync_Status(status)
                print("GEt device:",did,status)

    def Sync_Power(self,hid,did,power):
        print("Sync stt:",did)
        for dev in self.tab2.ldevice:
            if dev.Get_Device_Id()==did:
                dev.Sync_Power(power)
                print("GEt device:",did,power)

    def Init_UI(self):
        QToolTip.setFont(QFont('SansSerif', 10))

        # Init Tab Widget
        self.tab =tabwg.fancyTabWidget(self)

        # Create Tab
        self.tab1 = QLabel("tab 1")
        self.tab2 = ctrlwg.controlWidget()
        self.tab3 = QLabel("tab 3")
        self.tab4 = QLabel("tab 4")
        self.tab5 = QLabel("tab 5")

        self.tab2.btn_click_sig.connect(self.Btn_Press)

        self.tab.insertTab(0, self.tab1, QIcon("img/home.png"), "Home")
        self.tab.insertTab(1, self.tab2, QIcon("img/control.png"), "Control")
        self.tab.insertTab(2, self.tab3, QIcon("img/time.png"), "History")
        self.tab.insertTab(3, self.tab4, QIcon("img/timer.png"), "Timer")
        self.tab.insertTab(4, self.tab5, QIcon("img/setting.png"), "Setting")

        self.tab.setCurrentIndex(0)
        self.tab.setTabEnabled(3, False)
        self.tab.setTabEnabled(4, False)

        self.setCentralWidget(self.tab)

        self.show()

    def Btn_Press(self,hid, did, status):
        print("btn pressss",hid,did,status)
        self.btn_click_sig.emit(hid,did,status)
        #conn.updateStatus(hid, did, status)

    def Update_All_Device(self,ldevice):
        #print("UPDATE ALL DEVICE",ldevice)
        self.tab2.Update_All_Device(ldevice)


class mqttThread (QThread):
    sync_sig = pyqtSignal(str, str, int)
    get_device_sig = pyqtSignal(list)

    def __init__(self):
        QThread.__init__(self)
        self.connected = 0
        self.data_latest=0

        self.Init()

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.Is_Connected)
        self.timer.start(1000)

        self.start()

    def Init(self):
        self.mqttc=mqtt.MqttClass()
        print("Check thread alive",self.mqttc.isAlive())

        self.mqttc._mqttc.on_message = self.Mqtt_On_Message
        self.mqttc._mqttc.on_connect = self.Mqtt_On_Connect
        self.mqttc._mqttc.on_disconnect = self.Mqtt_On_Disconnect
        self.mqttc.start()


    def Is_Connected(self):
        if self.connected != 1 :
            self.Init()
            print("Try to connect to server")
        else:
            if self.data_latest!=1:
                self.mqttc.Sub_All()
                self.mqttc.publish("H1/GETDEVICE", json.dumps({"hid": "H1"}))
                print("Connected to server")
            else:
                self.timer.stop()
                print("Latest data")



    def Mqtt_On_Connect(self, mqttc, obj, flags, rc):
        print("on connect with rc: "+str(rc))
        self.connected=1
        self.Is_Connected()

    def Mqtt_On_Disconnect(self,client, userdata, rc):
        print("disconnect")
        self.connected = 0
        self.data_latest = 0
        print("mat ket noi")



    def Mqtt_On_Message(self, mqttc, obj, msg):
        self.connected = 1
        self.data_latest = 0
        print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload.decode("utf-8")))
        _msg = str(msg.payload.decode("utf-8"))
        # print(json.loads(x))
        try:
            _content = json.loads(_msg)

            case = msg.topic[msg.topic.find('/') + 1:]
            cid = msg.topic[:msg.topic.find('/')]
            if case == "SCONTROL":
                print("control!!!")

                hid=_content["hid"]
                did=_content["did"]
                status=_content["status"]
                self.Control_Handle(hid,did,status)
                #json_data = json.dumps(rsp)
                #self.mqttc.publish(_content["hid"]+"/CONTROL",json_data)

            if case=="RGETDEVICE":
                if _content["rcode"]==200:
                    self.data_latest=1
                    self.Get_Device_Handle(_content["ldevice"])

            if case == "SCONTROLS":
                print("Scontrols",_content)
                hid = _content["hid"]
                list_device=_content["devices"]
                for dev in list_device:
                    self.Control_Handle(hid, dev["did"], dev["status"])

        except:
            print("pare json fail")

    def Control_Handle(self,hid,did,status):

        self.sync_sig.emit(hid,did,status)
        #pl = '@ZBC='+str(data["did"])+','+str(data["status"])+'!'

        #self.uart.Serial_Write(pl)

    def Get_Device_Handle(self,ldevice):
        print(ldevice)
        self.get_device_sig.emit(ldevice)


class homeController (QThread):
    global gui

    def __init__(self):
        QThread.__init__(self)
        self.conn = mqttThread()
        self.uart = uartThread()

        # Create connection to sql lite
        self.db=db.database()
        self.db.start()
        create_table=self.db.createTable()
        if create_table==None:
            self.Init_Device_For_Gui()

        self.Init()

        self.start()

    def Init(self):
        gui.btn_click_sig.connect(self.Button_Click_Update_Status)
        self.conn.sync_sig.connect(self.Sync_Status)
        self.conn.get_device_sig.connect(self.Get_Device)
        self.uart.ctrl_sig.connect(self.Update_Status_From_ZB)
        self.uart.get_all_device_sig.connect(self.Get_All_Device_Handle)
        self.uart.uart_update_power.connect(self.Uart_Update_Power)


    def Init_Device_For_Gui(self):
        ldevice=self.db.findDevices()
        gui.Update_All_Device(ldevice)

    def Uart_Update_Power(self,addr,power):
        print(addr)
        hid,pow=self.db.updatePowerToDb(addr,power)
        gui.Sync_Power(hid,addr,pow)
        pay=json.dumps({"hid": hid,"did": addr,"power": pow})
        self.conn.mqttc.publish("H1/UPDATEPOWER",pay)

    def Get_All_Device_Handle(self,list):
        print("from main control, get all dev")
        for dev in list:
            ret = self.db.findDevice(dev["addr"])
            if ret!=None:
                if ret["status"]!=dev["status"]:
                    self.db.updateStatusToDb(ret["did"],dev["status"])
                    gui.Sync_Status(ret["hid"],ret["did"],dev["status"])
                    self.Update_Status_To_Server("H1",ret["did"],dev["status"])

    def Update_Status_From_ZB(self,did,status):
        self.db.updateStatusToDb(did,status)
        gui.Sync_Status("H1", did, status)
        self.conn.mqttc.publish("H1/CONTROL",self.Create_Control_Payload("H1",did,status))


    def Sync_Status(self, hid, did, status):

        gui.Sync_Status(hid,did,status)
        self.uart.Send_Control_Signal_To_Serial(did,status)

    def Get_Device(self,ldevice):
        #print("from controller",ldevice)
        #self.db.findDevices()
        #self.db.updateStatusToDb("00124B000A91644B1",1)
        for dev in ldevice:
            self.db.insertDevice(dev["did"],dev["dname"],dev["status"],dev["type"],dev["hid"])

        for dev in ldevice:
            ret=self.db.findDevice(dev["did"])
            if ret!=None:
                if ret["status"]!=dev["status"]:
                    self.Update_Status_To_Server(ret["hid"],ret["did"],ret["status"])
                    dev["status"]=ret["status"]

        #gui.Update_All_Device(ldevice)
        #self.Init_Device_For_Gui()

    def Button_Click_Update_Status(self,hid,did,status):
        self.uart.Send_Control_Signal_To_Serial(did,status)

    def Update_Status_To_Server(self,hid,did,status):

        self.conn.mqttc.publish(hid + "/CONTROL",self.Create_Control_Payload(hid,did,status))
        gui.Sync_Status(hid,did,status)

    def Create_Control_Payload(self,hid,did,status):
        payload={}
        payload['hid']=hid
        payload['did']=did
        payload['status']=status
        return json.dumps(payload)

app = QApplication(sys.argv)
app.setApplicationName("IOT Gateway")
gui = mainWindow()
gui.setWindowTitle("IOT Gateway")
main=homeController()
palette = QPalette()

gui.setPalette(palette)
#gui.showFullScreen()
#gui.setCursor(QCursor(Qt.BlankCursor))
main.start()
sys.exit(app.exec_())
