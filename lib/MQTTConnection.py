import paho.mqtt.client as mqtt
import threading
import json
import ssl

class MqttClass (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self._mqttc = mqtt.Client("4546")

        self._mqttc.on_publish = self.mqtt_on_publish
        self._mqttc.on_subscribe = self.mqtt_on_subscribe
        self.clientid="123"

        # Set LWT message
        self.lwt={}
        self.lwt["hid"]="H1"
        self._mqttc.will_set("H1/HGOOFF", json.dumps(self.lwt), 0, False)

    def mqtt_on_publish(self, mqttc, obj, mid):
        print("mid: "+str(mid))
    def mqtt_on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("Subscribed: "+str(mid)+" "+str(granted_qos))
    def publish(self,topic,msg):
         self._mqttc.publish(topic,msg)
    def Sub_All(self):
        self._mqttc.subscribe("#", 0)
    def run(self):
        try:
            self._mqttc.connect("127.0.0.1", 1883,30)
        except:
            print("No connection")
            return None
        self._mqttc.subscribe("#", 0)
        self.publish("H1/HGOON",json.dumps(self.lwt))
        rc = 0
        while rc == 0:
            rc = self._mqttc.loop_forever()
        return rc

