#!/usr/bin/python

import sqlite3
import threading
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class database (QThread):
    def __init__(self):
        QThread.__init__(self)
        self.conn = sqlite3.connect('test.db')
        print("Opened database successfully")

    def createTable(self):
        try:
            self.conn.execute('''CREATE TABLE DEVICE
                   (ID CHAR(50) PRIMARY KEY     NOT NULL,
                   NAME           CHAR(50)    NOT NULL,
                  STATUS            INT     NOT NULL,
                   TYPE       INT NOT NULL,
                   HID       CHAR(50) NOT NULL,
                   POWER INT DEFAULT 0
                   );''')
            return 1
        except:
            print("Create db fail")
            return None
    def insertDevice(self,did, dname, status, type,hid):
        value = (did, dname, status, type,hid)
        print(value)
        try:
            self.conn.execute("INSERT INTO DEVICE (ID,NAME,STATUS,TYPE,HID) VALUES (?,?,?,?,?)", value);
        except:
            print("fail")

        self.conn.commit()
        print("Records created successfully")

    #insertDevice("D3", "den 2", 1, 1)

    def findDevices(self):
        cursor = self.conn.execute("SELECT id, name,status,type,hid,power from DEVICE")
        ldevice=[]
        for data in cursor:
            print(data)
            ldevice.append({"did": data[0], "dname": data[1], "status": data[2], "type": data[3], "hid": data[4],"power": data[5]})
        return ldevice

    def findDevice(self,did):
        cursor = self.conn.execute("SELECT id, name,status,type,hid from DEVICE WHERE id = '%s'" % did)
        data = cursor.fetchone()
        if data is None:
            print('There is no component named ')
            return None
        else:
            print('Component found with rowid', data)
            return {"did": data[0],"dname":data[1],"status":data[2],"type": data[3],"hid": data[4]}

    def updateStatusToDb(self,id,status):
        print("update:",id,status)
        self.conn.execute("UPDATE DEVICE SET status = ? WHERE id = ?",(status,id,))
        self.conn.commit()

    def updatePowerToDb(self,did,power):
        print("update:",did,power)
        cursor = self.conn.execute("SELECT id,power,hid from DEVICE WHERE id = '%s'" % did)
        data = cursor.fetchone()
        if data is None:
            print('There is no component named ')
            return None
        else:
            print("find:",data[0],data[1])
            self.conn.execute("UPDATE DEVICE SET power = ? WHERE id = ?", (power+data[1], did,))
            return data[2],power+data[1]
            self.conn.commit()

'''
x=database()
x.start()
x.createTable()
x.insertDevice('00124B000A91EF1D4', 'Đèn 8', 0, 1,"H1")
x.updateStatusToDb("D2",9)
x.findDevices()
#print(x.findDevice("'))
'''
