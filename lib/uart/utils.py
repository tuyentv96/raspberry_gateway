def Parse_To_Cmd(data):

    # Check data contain ZBR
    if data.find('ZBR')<0:
        return None

    #str="@ZBR=1;123456;10!"

    # 1;123456;10!

    cmd_index = data.find(';')

    # cmd=1
    cmd = data[cmd_index-1]

    # addr=123456
    addr_index = data.find(';', cmd_index + 1)

    addr = data[cmd_index + 1:addr_index]

    val_index = data.find(';', addr_index )

    # val= 2;10,21
    val = data[val_index + 1:len(data)-1]

    return {"cmd": int(cmd), "addr": addr, "val":val}



def Zigbee_Control(addr,val):
    print("Zigbee_Control")
    status_offset=int(val)

    offset = int(status_offset/10)
    status = int(status_offset % 10)

    return {"addr":addr+str(offset),"status":status}

def Zigbee_Get_All_Device(addr,val):
    print("Zigbee Get all Device")

    #val=3,10,21,31

    sum_index=val.find(";")
    sum=val[:sum_index]
    #print(int(sum))

    val=val[sum_index+1:]+";"
    # 10,21,31
    list_dev=[]
    #print("+++++")

    for x in range(0,int(sum)):
        #print(x)
        i=val.find(";")
        j=int(val[:i])
        val=val[i+1:]
        offset=int(j/10)
        status=int(j%10)
        #print(i,j,val,"---")
        list_dev.append({"addr":(addr+str(offset)),"status":status})

    print(list_dev)
    return list_dev

def Zigbee_Update_Power(addr,val):
    index=val.find(",")
    pos=val[:index]
    power=val[index+1:]

    if power=="" or pos=="":
        return None

    return {"addr":addr+pos,"power": int(power)}

'''

ex="@ZBR=1;00124B000A91644B;3;10;21;31!"
ex="@ZBR=6;00124B000A91EF1D;1,2!"



op5="@ZBR=5,00124B000A91EF1D,10!"

x=Parse_To_Cmd(ex)
print(x)

if x!=None:
    if x["cmd"]==1:
        ret=t.Zigbee_Get_All_Device(x["addr"],x["val"])
        print(x)
    if x["cmd"]==5:
        ret=t.Zigbee_Control(x["addr"],x["val"])
        print(ret)
    if x["cmd"]==6:
        ret=t.Zibee_Update_Power(x["addr"],x["val"])
        print(ret)

'''

