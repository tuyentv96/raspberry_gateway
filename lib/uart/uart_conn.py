import serial
import threading
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

class Uart (QThread):
    uart_sig=pyqtSignal(int,name="uartsignal")
    uart_sig1 = pyqtSignal(int, name="uartsignal1")
    uart_ctr=pyqtSignal(str, name="uartctr")
    def __init__(self):
        super(Uart, self).__init__()
        self.uart_port="COM5"
        #self.uart_port = "/dev/ttyUSB0"
        self.uart_conn = 0
        self.uart_timer = QTimer(self)
        self.uart_sig.connect(self.reconnectUart)
        #self.setTimerUart()

    def setTimerUart(self):
        self.uart_timer.stop()
        self.uart_timer = QTimer(self)
        self.uart_timer.timeout.connect(self.tryOpenSerial)
        self.getSerialOrNone(self.uart_port)
        self.uart_timer.start(1000)

    def reconnectUart(self,cmd):
        self.setTimerUart()

    def tryOpenSerial(self):
        print("uart_conn:",self.uart_conn)
        if self.uart_conn==1:
            self.uart_timer.stop()
            self.start()
            self.uart_sig1.emit(6) #Getalldevice
            return
        if self.getSerialOrNone(self.uart_port)==None:
            print("try to open uart")
            self.uart_sig.emit(0)

        else:
            self.uart_timer.stop()
            #self.uart_sig.emit(6) #Getalldevice
            #self.Get_All_Device()
            #self.uart_sig.emit(1)
            self.start()
            self.uart_sig1.emit(6)  # Getalldevice
    def setTimer(self):
         self.uart_timer.start(1000)

    def getSerialOrNone(self,port):
        try:
            self.ser = serial.Serial(port,115200)  # Open named port
            #self.ser.baudrate = 115200  # Set baud rate to 115200
            self.ser.close()
            self.ser.open()
            self.uart_conn=1
            return 1
        except serial.SerialException as e:
            print(e)
            return None

    def run(self):
        print("Uart start")
        buffer = ""
        try:
            while True:
                data = self.ser.read()
                try:
                    char = data.decode('ascii')
                    print(char)
                    buffer += char
                    if char == '!':
                        print(buffer)
                        #self.handle(buffer)
                        self.uart_ctr.emit(buffer)
                        buffer = ""
                except:
                    print("Decode fail")
                    buffer = ""
        except:
            print("uart connection is break")
            self.uart_conn = 0
            self.uart_sig.emit(2)
            return


    def Serial_Write(self, data):
        print("Serial writeeee",data)
        try:
            self.ser.write(data.encode('ascii'))
        except:
            print("uart not ready")