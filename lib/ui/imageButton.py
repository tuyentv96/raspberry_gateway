from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import lib.ui.img as img


class imageButton(QLabel):
    btn_click_sig = pyqtSignal(int,name="btnclick")
    #sigupdate = pyqtSignal(int,name="sync")
    def __init__(self,status):
        super(imageButton, self).__init__()

        self.status = status
        self.Set_Button()
        #self.sigupdate.connect(self.Update_Status_Button)
        self.click=0


    def Set_Button(self):
        print("Set_Button:",self.status)
        if self.status == 1:
            self.setPixmap(QPixmap('img/on.png'))
        else:
            self.setPixmap(QPixmap('img/off.png'))

    def Update_Status_Button(self, status):

        self.status = status
        print("update stt", self.status)
        self.Set_Button()


    def mouseReleaseEvent(self, event):
        if self.status == 1:

            self.send_status = 0
        else:
            self.send_status = 1

        #self.Update_Status_Button(self.send_status)
        print("from img ", self.send_status)
        self.btn_click_sig .emit(self.send_status)


class devicePanel(QWidget):
    sync_sig = pyqtSignal(str,str,int,name="sync")
    btnsig = pyqtSignal(str,str, int)
    def __init__(self, hid,did,name,type,status,power):
        super(devicePanel, self).__init__()
        self.did = did
        self.hid = hid
        self.type = type
        self.name = name
        self.power = power
        self.status = status



        self.textQVBoxLayout = QVBoxLayout()
        self.textUpQLabel = QLabel()
        self.textDownQLabel = QLabel()

        self.button = imageButton(self.status)

        self.button.btn_click_sig.connect(self.Send_Status)


        self.textQVBoxLayout.addWidget(self.textUpQLabel)
        self.textQVBoxLayout.addWidget(self.textDownQLabel)

        self.allQHBoxLayout = QHBoxLayout()
        self.iconQLabel = QLabel()

        self.Set_Text_Up(name)
        self.Set_Text_Down(self.power)
        self.Set_Button(status)

        self.allQHBoxLayout.addWidget(self.iconQLabel, 0)
        self.allQHBoxLayout.addLayout(self.textQVBoxLayout, 1)
        self.allQHBoxLayout.addWidget(self.button, 2)

        self.allQHBoxLayout.setAlignment(self.button, Qt.AlignRight)

        self.setLayout(self.allQHBoxLayout)

        # setStyleSheet
        self.textUpQLabel.setStyleSheet('''
            color: rgb(0, 0, 0);
        ''')
        self.textDownQLabel.setStyleSheet('''
            color: rgb(255, 0, 0);
        ''')

    def Get_Device_Id(self):
        return self.did

    def Get_Status_Device(self):
        return self.did,self.status


    def Set_Text_Up(self, text):
        myFont = QFont("Arial", 15, QFont.Bold)
        self.textUpQLabel.setFont(myFont)

        self.textUpQLabel.setText(text)

    def Set_Text_Down(self, text):
        self.textDownQLabel.setText(str(text) + " W")

    def Set_Icon(self, imagePath):
        self.iconQLabel.setPixmap(QPixmap(imagePath).scaled(64, 64, Qt.KeepAspectRatio))

    def Sync_Status(self,status):
        print("testing")
        if status == 1:
            if self.type == 1:
                self.Set_Icon(('img/icon1.png'))

            else:
                self.Set_Icon(('img/led_on.png'))
        else:
            if self.type == 1:
                self.Set_Icon(('img/icon.png'))
            else:
                self.Set_Icon(('img/led_off.png'))
        self.button.Update_Status_Button(status)

    def Set_Button(self, status):
        self.Sync_Status(status)
        #self.btnsig.emit(self.hid,self.did,status)

    def Send_Status(self,status):
        self.sync_sig.emit(self.hid, self.did, status)
        print("status:",status)

    def Sync_Power(self,power):
        self.power=power
        self.Set_Text_Down(power)







