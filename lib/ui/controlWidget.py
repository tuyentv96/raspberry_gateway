import sys

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import lib.ui.imageButton as imgbtn


class controlWidget(QWidget):
    btn_click_sig=pyqtSignal(str,str,int,name="btnsig")
    def __init__ (self,_data=None):
        super(controlWidget, self).__init__(None)

        self.ldevice=[]

    def Update_All_Device(self,ldevice):
        print("From tab2",ldevice)
        self.ldevice.clear()
        for dev in ldevice:
            img_btn=imgbtn.devicePanel(dev["hid"],dev["did"],dev["dname"],dev["type"],dev["status"],dev["power"])
            img_btn.sync_sig.connect(self.Emit_To_External)
            self.ldevice.append(img_btn)
        self.Set_MainLayout()

    def Emit_To_External(self,hid,did,status):
        print("emit to exx",hid,did,status)
        self.btn_click_sig.emit(hid,did,status)

    def Set_MainLayout(self):
        vbox = QVBoxLayout(self)
        vbox1 = QVBoxLayout(self)
        i=int((len(self.ldevice)/2)+len(self.ldevice)%2)
        for index,dev in enumerate(self.ldevice):
            #print(dev,"index",index)
            if index<i:
                vbox.addWidget(dev)
                print(index)
            else:
                vbox1.addWidget(dev)
                print(index)

        wg1=QWidget()
        wg1.setStyleSheet("background-color: rgb(224,224,224); margin:1px; ")
        wg1.setLayout(vbox)

        wg2=QWidget()
        wg2.setStyleSheet("background-color: rgb(224,224,224); margin:1px; ")
        wg2.setLayout(vbox1)

        mainlayout = QHBoxLayout(self)

        mainlayout.addWidget(wg1)
        mainlayout.addWidget(wg2)
        self.setLayout(mainlayout)
